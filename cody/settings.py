"""
    settings.py
    ~~~~~~~~~~~

    ``cody`` implements 3 layers of settings, each layer having higher
    precedens than the previous. Layer 1 is default settings. Layer 2
    is system settings. These are configured by whoever packages
    ``cody``. If you use the Docker image, the system settings will be
    settings appropriate for running in Docker. Layer 3 is the user
    settings.  These are settings configured by whoever hosts cody.
    The paths for these files can be configured by the environment
    variables ``CODY_SYSTEM_CONFIG_PATH`` and ``CODY_USER_CONFIG_PATH``.

    This module exposes one function, ``load``, used to read the
    configuration from layer 2 and 3 as described above.

    The file format is TOML, and here is the full structure:
    ``` toml
    metrics = true

    [matrix]
    server = "https:// the server where your bot is registered"
    username = "the username that cody will act as"
    password = "the password for that user"
    ```
"""

import copy
import logging
import pprint
import sys

import toml


logger = logging.getLogger(__name__)


def _dict_merge(old, new):
    for key, value in new.items():
        if isinstance(value, dict) and key in old:
            _dict_merge(old[key], value)
        else:
            old[key] = new[key]


def _read_config(config_path):
    try:
        with open(config_path) as f:
            content = f.read()
    except FileNotFoundError as err:
        logger.critical("%s: %r", err.strerror, err.filename)
        sys.exit(4)
    try:
        return toml.loads(content)
    except toml.TomlDecodeError:
        logger.critical("%s contains invalid TOML")
        sys.exit(5)


def load(system_config_path, user_config_path):
    config = {}

    if system_config_path:
        logger.info("Reading system config from %s", system_config_path)
        config.update(_read_config(system_config_path))

    if user_config_path:
        logger.info("Reading user config from %s", user_config_path)
        _dict_merge(config, _read_config(user_config_path))

    safe_config = copy.deepcopy(config)
    safe_config["matrix"]["password"] = "********"
    logger.debug("Config:\n%s.", pprint.pformat(safe_config))
    logger.info("Config: %s.", safe_config)

    return config
