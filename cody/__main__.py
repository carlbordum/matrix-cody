"""
    cody
    ~~~~

    ``cody`` is a matrix bot that evaluates your code!

    Currently, ``cody`` can evaluate Python, Ruby and Javascript, but
    support for more languages is on the roadmap.

    For configuring ``cody``, please refer to ``cody/settings.py``.

    Read the source at: https://gitlab.com/carlbordum/matrix-cody

    Enjoy!
"""

import asyncio
from functools import partial
import logging
import os

from nio import AsyncClient, RoomMessageText, InviteMemberEvent
from prometheus_client import start_http_server

import languages
import settings


logger = logging.getLogger(__name__)
log_format = "[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s"
logging.basicConfig(format=log_format, level=logging.INFO)


async def callback(chroot_dir, client, room, event):
    logger.debug(
        "Message received from %s in room %s",
        room.user_name(event.sender),
        room.display_name,
    )

    message = event.body
    language = languages.from_message(chroot_dir, message)
    if language is None:
        return

    try:
        result = await languages.safe_eval(language)
    except languages.SafeEvalError as e:
        if language.quiet_on_failure:
            return
        result = str(e)

    await client.room_send(
        room_id=room.room_id,
        message_type="m.room.message",
        content={
            "msgtype": "m.text",
            "body": f"```\n{result}\n```",
            "format": "org.matrix.custom.html",
            "formatted_body": f"<code>{result}</code>",
        },
    )


async def auto_join_callback(client, room, event):
    """Automatically join all rooms we are invited to."""
    if client.user_id == event.state_key and event.membership == "invite":
        await client.join(room.room_id)
        logger.info(
            f"Joined room {room.display_name!r}({room.room_id}) invited by {event.sender}"
        )


async def main():
    system_config_path = os.getenv("CODY_SYSTEM_CONFIG_PATH", False)
    user_config_path = os.getenv("CODY_USER_CONFIG_PATH", False)
    config = settings.load(system_config_path, user_config_path)

    if config["metrics"]:
        start_http_server(port=8000)

    client = AsyncClient(config["matrix"]["server"], user=config["matrix"]["username"])

    await client.login(config["matrix"]["password"])
    # Sync before we register functional callback, so we dont react to old messages
    # We still want to join all rooms.
    client.add_event_callback(partial(auto_join_callback, client), InviteMemberEvent)
    await client.sync()

    client.add_event_callback(
        partial(callback, config["chroot_dir"], client), RoomMessageText
    )

    await client.sync_forever(timeout=30000)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
