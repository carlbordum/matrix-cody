"""
    languages.py
    ~~~~~~~~~~~~

    This module is concerned with evaluating code from untrusted
    sources. Want to add support for a new language? See the
    ``Language`` class below.
"""

import ast
import asyncio
import time

from prometheus_client import Counter, Summary


EVAL_TIME = Summary("cody_eval_seconds", "Time spent evaluating code", ["language"])
EXCEPTION_COUNT = Counter(
    "cody_eval_exceptions", "Number of exceptions when evaluating code", ["language"]
)


class SafeEvalError(Exception):
    pass


def MiB(mib):
    """Convert MiBs to bytes and return as a string."""
    return str(mib * 1024 * 1024)


class Language:
    """Subclasses of this class represents the different languages
    `cody` supports. A language consists of various hooks. Subclasses
    should implement at least the properties `command` and
    `send_to_stdin`.

    Here is a list of all the hooks and a short description:
     * `check_syntax` <function>: raise SyntaxError if syntax of
       `self.code` is invalid. Default no-op. Optional.
     * `command` <list of str>: `nsjail` command to execute. Required.
     * `send_to_stdin` <bytes>: whatever should be written to stdin
                                before reading stdout. Required.
     * `parse_output` <function>: if the output (stdout) needs some
        reformatting, this is where it's done. Optional.

    The exact flow can be seen in `safe_eval` below.
    """

    def __init__(self, code, *, chroot_dir, timeout=3, quiet_on_failure=False):
        self.code = code
        self.chroot_dir = chroot_dir
        self.timeout = timeout
        self.quiet_on_failure = quiet_on_failure
        self.name = self.__class__.__name__

    def check_syntax(self):
        return  # no-op

    def parse_output(self, output):
        return output  # no-op

    async def safe_eval(self):
        """This method implements the "flow" when evaluating a language.

        Please use the convenience module-level wrapper below, that
        also records metrics.
        """
        try:
            self.check_syntax()
        except SyntaxError:
            return "Invalid Syntax"

        proc = await asyncio.create_subprocess_exec(
            *self.command,
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        stdout, stderr = await proc.communicate(self.send_to_stdin)

        if proc.returncode == 137:
            err = "process exited with status 137, likely timed out or memory limit was exceeded"
            raise SafeEvalError(err)
        if proc.returncode != 0:
            raise SafeEvalError(f"process exited with status: {proc.returncode}")

        result = self.parse_output(stdout.strip().decode())
        return result.strip()


class Python(Language):
    @property
    def send_to_stdin(self):
        return self.code.encode() + b"\n"

    @property
    def command(self):
        return [
            "nsjail",
            "-Mo",
            "--chroot",
            self.chroot_dir,
            "-E",
            "LANG=en_US.UTF-8",
            "-E",
            "LD_LIBRARY_PATH=/usr/local/lib/",
            "-R/usr",
            "-R/lib",
            "-R/lib64",
            "--user",
            "nobody:matrix-user",
            "--group",
            "nogroup:nogroup",
            "--time_limit",
            str(self.timeout),
            "--disable_proc",
            "--iface_no_lo",
            "--cgroup_mem_max",
            MiB(10),
            "--cgroup_pids_max",
            "1",
            "--quiet",
            "--",
            "/usr/local/bin/python",
            "-ISqiu",
        ]

    def check_syntax(self):
        ast.parse(self.code)


class Ruby(Language):
    @property
    def send_to_stdin(self):
        return self.code.encode() + b"\n"

    @property
    def command(self):
        return [
            "nsjail",
            "-Mo",
            "--chroot",
            self.chroot_dir,
            "-R/usr",
            "-R/lib",
            "-R/lib64",
            "--user",
            "nobody:matrix-user",
            "--group",
            "nogroup:nogroup",
            "--time_limit",
            str(self.timeout),
            "--disable_proc",
            "--iface_no_lo",
            "--cgroup_mem_max",
            MiB(10),
            "--cgroup_pids_max",
            "2",
            "--quiet",
            "--",
            "/usr/bin/irb",
            "-f",
            "--noprompt",
        ]

    def parse_output(self, output):
        # I think there is some bug becuase ``irb`` does not detect a
        # PTY. stdout seems to be the concatenation of a warning,
        # followed by the code we sent to stdin, and then finally the
        # result. This may break if you send something to stdin such
        # that it is evaluated before the rest of stdin is read.
        try:
            idx = output.index(self.code)
        except ValueError:
            raise SafeEvalError("failed to parse result")
        return output[idx + len(self.code) :]


class NodeJS(Language):
    @property
    def send_to_stdin(self):
        return None

    @property
    def command(self):
        return [
            "nsjail",
            "-Mo",
            "--chroot",
            self.chroot_dir,
            "-R/usr",
            "-R/lib",
            "-R/lib64",
            "--user",
            "nobody:matrix-user",
            "--group",
            "nogroup:nogroup",
            "--time_limit",
            str(self.timeout),
            "--disable_proc",
            "--iface_no_lo",
            "--cgroup_mem_max",
            MiB(10),
            "--cgroup_pids_max",
            "6",
            "--quiet",
            "--",
            "/usr/bin/nodejs",
            "--print",
            self.code,
        ]


class Units(Language):
    def __init__(self, from_unit, to_unit, *, chroot_dir, timeout=3):
        code = f"{from_unit} to {to_unit}"
        super().__init__(
            code, chroot_dir=chroot_dir, timeout=timeout, quiet_on_failure=True
        )
        self.from_unit = from_unit
        self.to_unit = to_unit

    @property
    def send_to_stdin(self):
        return None

    @property
    def command(self):
        return [
            "/usr/bin/units",
            "--compact",
            "--one-line",
            "--quiet",
            self.from_unit,
            self.to_unit,
        ]

    def parse_output(self, output):
        return f"{self.from_unit} = {output} {self.to_unit}"


def _try_units(code, chroot_dir, timeout):
    args = code.split(" to ")
    if len(args) != 2:
        args = code.split(" in ")
        if len(args) != 2:
            return
    from_unit, to_unit = args
    return Units(from_unit, to_unit, chroot_dir=chroot_dir, timeout=timeout,)


async def safe_eval(lang: Language):
    """Call ``lang.safe_eval``` and record metrics."""
    start_time = time.monotonic()
    try:
        return await lang.safe_eval()
    except Exception:
        EXCEPTION_COUNT.labels(lang.name).inc()
        raise
    finally:
        EVAL_TIME.labels(lang.name).observe(time.monotonic() - start_time)


def from_message(chroot_dir, message):
    if message.startswith("!py"):
        return Python(message[3:].strip(), chroot_dir=chroot_dir)
    if message.startswith("!rb"):
        return Ruby(message[3:].strip(), chroot_dir=chroot_dir)
    if message.startswith("!js"):
        return NodeJS(message[3:].strip(), chroot_dir=chroot_dir, timeout=4)
    return _try_units(message.strip(), chroot_dir=chroot_dir, timeout=2)
