#!/bin/sh

set -e

# Set up NSJAIL memory/pids cgroups
mkdir /sys/fs/cgroup/memory/NSJAIL
mkdir /sys/fs/cgroup/pids/NSJAIL
# On first run, nsjail makes some stuff that it does not have permission to
# when we execute code as matrix-user.
nsjail -Mo --chroot /code/chroot -E LANG=en_US.UTF-8 -E LD_LIBRARY_PATH=/usr/local/lib/ -R/usr -R/lib -R/lib64 --time_limit 1 --disable_proc --iface_no_lo --cgroup_mem_max 10485760 --cgroup_pids_max 1 --really_quiet -- /usr/local/bin/python -c "1"

exec "$@"
