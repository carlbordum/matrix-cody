# Hacking

Contributions are very welcome! If you do not know what to contribute, consider
adding support for a new language or check the open issues:
https://gitlab.com/carlbordum/matrix-cody/-/issues


## Getting Started

To start hacking on `cody` you need [git](https://git-scm.com/downloads),
[docker](https://docs.docker.com/engine/install/) and
[docker-compose](https://docs.docker.com/compose/install/).

First, clone the repo:

    $ git clone https://gitlab.com/carlbordum/matrix-cody/
    $ cd matrix-cody

Second, start the environment:

    $ docker-compose up -d


## The environment

The development environment contains a few services. The bread and butter is a
local synapse accessible at localhost:8008 and matrix client (riot.im) at
localhost:8081. Two users are registered here: "dev" and "cody" with the
passwords "dev" and "cody", respectively. cody is configured to authenticate as
the cody user, and you are intended to use the user "dev". Try to log in with
"dev:dev", make an unencrypted room and invite the user "@cody:localhost:8008".

Furthermore, prometheus (configured to scrape cody) is running at port 9090 and
Grafana is running at port 3000. Try to go to localhost:3000 and log in as
admin:admin.


## Useful commands

After making changes, restart cody with:

    $ docker-compose restart cody

To read the logs, you can:

    $ docker-compose logs cody

To run the unit tests:

    $ docker-compose exec cody pytest
