## 0.8.0

- Support for `units` conversions
- Code coverage in CI
- Three flake8 plugins in CI for less complexity and security

## 0.7.0

- Metrics got fixed this release
- grafana/ moved to dev-environment/
- New, pretty dashboards
- Grafana hosted with anonymous access
- Grafana graphs embedded in #cody:bordum.dk

## 0.6.0

- Added support for Javascript (nodejs) with `!js`
- With the introduction of js, we passed the rule of three, and therefore I
  introduced an abstraction for languages. Major refactor, simpler code
- McCabe (cyclomatic) complexity reduced and low limit set in CI
- Documentation for configuration improved
- Python subdependencies pinned (with pip-tools), so build is more reproducible

## 0.5.0

- Added support for Ruby with `!rb`

## 0.4.0

- Evaluates Python async
- pantalaimon included in dev environment to support e2ee
- hosted cody is also behind pantalaimon

## 0.3.0

- Cody now exports simple metrics
- Grafana and prometheus are part of the dev environment with a predefined
  Grafana dashboard
- Cody will now report syntax errors instead of an empty response
- Response is now backticked/<code>
- Response works with multiline results

## 0.2.0

- Features a complete cody/synapse/riot development environment built
  with `docker-compose`.
- Cody now automatically join rooms when invited.

## 0.1.0

- Initial release.
- This version can execute Python, is unit tested (with CI) and released
  automatically to Docker Hub.
