import pytest

from cody.languages import Ruby, safe_eval, SafeEvalError


pytestmark = pytest.mark.asyncio


async def ruby_eval(source_code, timeout=3):
    lang = Ruby(source_code, chroot_dir="/code/chroot", timeout=timeout)
    return await safe_eval(lang)


async def test_ruby_literal_int():
    assert await ruby_eval("4") == "4"


async def test_ruby_symbol():
    assert await ruby_eval(":ruby_is_in") == ":ruby_is_in"


async def test_ruby_literal_str():
    assert await ruby_eval("'Hello world!'") == '"Hello world!"'


async def test_ruby_literal_list():
    assert await ruby_eval("[1, 'two', 3, :four]") == '[1, "two", 3, :four]'


async def test_ruby_literal_dict():
    assert await ruby_eval("{1 => 'a', 2 => 'b'}") == '{1=>"a", 2=>"b"}'


async def test_ruby_math1():
    assert await ruby_eval("1337 + 45") == "1382"


async def test_ruby_math2():
    assert await ruby_eval("1 - 2 - 3") == "-4"


async def test_ruby_math3():
    assert await ruby_eval("1 - 2 * 3") == "-5"


async def test_ruby_lambda():
    assert await ruby_eval("(->(x) { x**5 }).call 5") == "3125"


async def test_ruby_list_range():
    assert await ruby_eval("a=*(0..4)") == "[0, 1, 2, 3, 4]"


async def test_ruby_list_comprehension():
    assert await ruby_eval("(0..4).map { |n| n**2 }") == "[0, 1, 4, 9, 16]"


async def test_ruby_simple_if():
    assert await ruby_eval("if 1 < 3 then 5 end") == "5"


async def test_ruby_time_limit():
    with pytest.raises(SafeEvalError):
        assert await ruby_eval("sleep 4", timeout=3)


async def test_ruby_exit_111():
    with pytest.raises(SafeEvalError, match=".*111.*"):
        assert await ruby_eval("Kernel.exit 111")


async def test_ruby_fork_bomb():
    # In Ruby, ``fork`` fails softly because ``pthread_create`` fails. This
    # tests ends with a timeout.
    with pytest.raises(SafeEvalError):
        assert await ruby_eval("loop { fork }") == ""
