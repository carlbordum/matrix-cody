import pytest

from cody.languages import from_message, Units, safe_eval


pytestmark = pytest.mark.asyncio


async def units_eval(source_code, timeout=3):
    lang = from_message("/code/chroot", source_code)
    assert isinstance(lang, Units)
    return await safe_eval(lang)


async def test_units_simple_1():
    assert await units_eval("10 kg to g") == "10 kg = 10000 g"


async def test_units_simple_2():
    assert (
        await units_eval("1 gallons + 3 pints to quarts")
        == "1 gallons + 3 pints = 5.5 quarts"
    )


async def test_units_simple_3():
    assert await units_eval("10 furlongs in miles") == "10 furlongs = 1.25 miles"
