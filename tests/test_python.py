import pytest

from cody.languages import Python, safe_eval, SafeEvalError


pytestmark = pytest.mark.asyncio


async def python_eval(source_code, timeout=3):
    lang = Python(source_code, chroot_dir="/code/chroot", timeout=timeout)
    return await safe_eval(lang)


async def test_literal_int():
    assert await python_eval("4") == "4"


async def test_literal_str():
    assert await python_eval("'Hello world!'") == "'Hello world!'"


async def test_literal_list():
    assert await python_eval("[1, 'two', 3, 'four']") == "[1, 'two', 3, 'four']"


async def test_literal_dict():
    assert await python_eval("{1: 'a', 2: 'b'}") == "{1: 'a', 2: 'b'}"


async def test_math1():
    assert await python_eval("1337 + 45") == "1382"


async def test_math2():
    assert await python_eval("1 - 2 - 3") == "-4"


async def test_math3():
    assert await python_eval("1 - 2 * 3") == "-5"


async def test_lambda():
    assert await python_eval("(lambda x: x**5)(5)") == "3125"


async def test_list_range():
    assert await python_eval("list(range(5))") == "[0, 1, 2, 3, 4]"


async def test_list_comprehension():
    assert await python_eval("[i**2 for i in range(5)]") == "[0, 1, 4, 9, 16]"


async def test_simple_if():
    assert await python_eval("if 1 < 3: 5") == "5"


async def test_time_limit():
    with pytest.raises(SafeEvalError):
        assert await python_eval("import time; repr(time.sleep(4))", timeout=3)


@pytest.mark.skip(reason="this test is flaky :(")
async def test_mem_limit():
    # Know how to write a faster or better mem test? Please do tell!
    # see https://gitlab.com/carlbordum/matrix-cody/-/issues/1
    with pytest.raises(SafeEvalError):
        assert await python_eval("a = 'A' * 512000000; a[-5:]", timeout=320) != "AAAAA"


async def test_exit_111():
    with pytest.raises(SafeEvalError, match=".*111.*"):
        assert await python_eval("import sys; sys.exit(111)")


async def test_fork_bomb():
    # timeout would raise exception
    assert await python_eval("while (os := __import__('os')): os.fork()") == ""
