import textwrap

import pytest

from cody.settings import load


def test_load_file_not_found():
    with pytest.raises(SystemExit) as e:
        load("/this/is/a/fake/path", "/another/fake/path")
    assert e.value.code == 4


def test_load_invalid_toml(tmp_path):
    f = tmp_path / "invalid.toml"
    f.write_text("a = 5\n[b = 6]")
    with pytest.raises(SystemExit) as e:
        load(str(f), False)
    assert e.value.code == 5


def test_load(tmp_path):
    system_settings = textwrap.dedent(
        """
    chroot_dir = "/hello/world"

    [matrix]
    username = "bad example"
    password = "hunter2"
    """
    )
    system = tmp_path / "system-settings.toml"
    system.write_text(system_settings)
    user = tmp_path / "user-settings.toml"
    user_settings = textwrap.dedent(
        """
    [matrix]
    password = "hunter3"
    """
    )
    user.write_text(user_settings)

    config = load(str(system), str(user))
    assert config["chroot_dir"] == "/hello/world"
    assert config["matrix"]["username"] == "bad example"
    assert config["matrix"]["password"] == "hunter3"
