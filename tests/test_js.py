import pytest

from cody.languages import NodeJS, safe_eval, SafeEvalError


pytestmark = pytest.mark.asyncio


async def js_eval(source_code, timeout=3):
    lang = NodeJS(source_code, chroot_dir="/code/chroot", timeout=timeout)
    return await safe_eval(lang)


async def test_js_literal_int():
    assert await js_eval("4") == "4"


async def test_js_literal_str():
    assert await js_eval("'Hello world!'") == "Hello world!"


async def test_js_literal_list():
    assert await js_eval("[1, 'two', 3, 'four']") == "[ 1, 'two', 3, 'four' ]"


async def test_js_literal_dict():
    assert (
        await js_eval("console.log({1: 'a', 2: 'b'})")
        == "{ '1': 'a', '2': 'b' }\nundefined"
    )


async def test_js_math1():
    assert await js_eval("1337 + 45") == "1382"


async def test_js_math2():
    assert await js_eval("1 - 2 - 3") == "-4"


async def test_js_math3():
    assert await js_eval("1 - 2 * 3") == "-5"


async def test_js_lambda():
    assert await js_eval("(x => x ** 5)(5)") == "3125"


async def test_js_list_range():
    assert await js_eval("Array.from({length: 5}, (x, i) => i)") == "[ 0, 1, 2, 3, 4 ]"


async def test_js_list_comprehension():
    assert await js_eval("[0,1,2,3,4].map(n => n**2)") == "[ 0, 1, 4, 9, 16 ]"


async def test_js_simple_if():
    assert await js_eval("if (1 < 3) {5} else {4}") == "5"


async def test_js_exit_111():
    with pytest.raises(SafeEvalError, match=".*111.*"):
        assert await js_eval("process.exit(111)")
