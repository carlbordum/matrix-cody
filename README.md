# matrix-cody

`matrix-cody` is a REPL for your [matrix](https://matrix.org) chat rooms.

This repository features a full development environment (`cody`, `synapse`,
`riot`) with `docker-compose` as well as `prometheus` and `grafana` with
predefined dashboards. Please see `HACKING.md` for instructions.
Contributions are very welcome and appreciated :-)

Chat with [@cody:bordum.dk](https://matrix.to/#/@cody:bordum.dk)!

Chat about [#cody:bordum.dk](https://matrix.to/#/#cody:bordum.dk)!


## Example session

    user> hello
    user> !py 1337
    cody> 1337
    user> !py 4 + 5
    cody> 9
    user> !py list(range(10))
    cody> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


## Supported languages

`cody` supports Python, Ruby and javascript with the message prefixes `!py`,
`!rb` and `!js`, respectively.


## Hosting

If you want to host `matrix-cody`, I recommend using the [docker
image](https://hub.docker.com/r/carlbordum/matrix-cody). It is continuously
pushed to Docker Hub. The `:latest` tag points to the latest versioned release
and the `:dev` tag points to the git master branch.

![](https://matrix.org/docs/projects/images//made-for-matrix.png)

`cody` is heavily inspired by the fun [pbot](https://github.com/raylu/pbot/).
